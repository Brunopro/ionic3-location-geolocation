import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    //para salvar localização
    private lat:number;
    private long:number;

    //para pedir permições
    private gotPosition: boolean = false;
    private gaveLocationPermission: boolean = false;
    private hasEnabledLocation: boolean = false;
    private doneChecking: boolean = false;
    private hasRequestedLocation: boolean = false;
  

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private diagnostic: Diagnostic
    ) {

  }

  getAuthorization() {


    let authorizationLoader = this.loadingCtrl.create({
      content: "Checando permissões..."
    });
    authorizationLoader.present();


    this.diagnostic.isLocationAuthorized().then((resp) => {
      authorizationLoader.dismiss().then(() => {
        this.gaveLocationPermission = resp;
        if (this.gaveLocationPermission) {
          let enablingLoader = this.loadingCtrl.create({
            content: "Checando status do GPS..."
          });
          enablingLoader.present();
          console.log('Gave location permission')
          this.diagnostic.isLocationEnabled().then((resp) => {
            console.log('Location is enabled : ' +resp)
            enablingLoader.dismiss()
            console.log('Enabling loader dismissed')
            this.doneChecking = true;
            this.hasEnabledLocation = resp;
            if (this.hasEnabledLocation) {
              this.getPosition();
            }
          });
        } else {
          this.gotPosition = false
          //this.resetUBSDistance()
          this.lat = null
          this.long = null
          if(!this.hasRequestedLocation) {
            this.hasRequestedLocation = true
            this.diagnostic.requestLocationAuthorization().then((res)=>{
              if(res!='denied'){
                console.log('Permission : '+ res)
                this.diagnostic.isLocationAuthorized().then((res)=>{
                  this.gaveLocationPermission = res;
                  if (this.gaveLocationPermission) {
                    let enablingLoader = this.loadingCtrl.create({
                      content: "Checando status do GPS..."
                    });
                  enablingLoader.present();
                    this.diagnostic.isLocationEnabled().then((resp) => {
                      enablingLoader.dismiss()
                      this.doneChecking = true;
                      this.hasEnabledLocation = resp;
                      if (this.hasEnabledLocation) {
                        this.getPosition();
                      }
                    });
                  }
                })
              } else { // denied location

              }
            })
          }
          this.doneChecking = true;
        }
      });
    }, err =>{
      authorizationLoader.dismiss()
      this.gotPosition = false
      //this.resetUBSDistance()
      this.lat = null
      this.long = null
    });
  }

  getPosition(){

  }



}
